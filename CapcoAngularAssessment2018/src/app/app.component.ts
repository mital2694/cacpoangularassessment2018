import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { PagerService } from './_services/pager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor (private httpService: HttpClient, private pagerService : PagerService) { }
  arrEmployees: string [];
  pager : any = {};
  pagedItems: any [];
  
  options = [5, 10, 15, 20];
  pageOptionSelected: number;

  onOptionSelected(event, currentPage){
    this.pageOptionSelected = event;
    this.setPage(currentPage);
  }

  ngOnInit() {
    this.pageOptionSelected = 5;
    // Added HttpClient Service to access the file and read its contents. 
    this.httpService.get('./assets/sample_data.json').subscribe (
      data => {
        // Fill the Array with Employees Data
        this.arrEmployees = data as string [];
        this.setPage(1);
      },
      (err : HttpErrorResponse) => {
        console.log(err.message);
      }
    )  
  }
  setPage(page : number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.arrEmployees.length, page, this.pageOptionSelected);
    console.log(this.pager);
    // get current page of items
    this.pagedItems = this.arrEmployees.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
